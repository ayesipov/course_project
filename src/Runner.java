import java.time.LocalDateTime;
import java.util.List;

public class Runner {

    public static void main(String[] args) {
        // Table TEST
        User user1 = new User("User1", "+380679200420");
        User user2 = new User("User2", "+380679200420");
        Table table1 = new Table(8);
        table1.addBooking(new Booking(LocalDateTime.of(2019, 4, 25, 15, 0, 0), LocalDateTime.of(2019, 4, 25, 16, 0, 0), "user1", user1));
        table1.addBooking(new Booking(LocalDateTime.of(2019, 4, 25, 19, 0, 0), LocalDateTime.of(2019, 4, 25, 20, 0, 0), "user2", user2));
        System.out.println("IS it Free? " + table1.isFree(LocalDateTime.of(2019, 4, 25, 16, 0, 0), LocalDateTime.of(2019, 4, 25, 18, 0, 0))); //IS it Free? true


        Table table2 = new Table(6);
        Table table3 = new Table(2);
        Booking order1 = new Booking(LocalDateTime.now(), LocalDateTime.now().plusHours(2), "hghgd", user1);
        Hall hall = new Hall("Hall", List.of(table1, table2, table3));

        //Hall TEST
        for (Table table : hall) {
            if (table.isFree(order1.getFrom(), order1.getTo())) {
                System.out.println("Table with number: " + table.getTableNumber() + " is free");
                table.addBooking(order1);
                break;
            }
        }

        for (Table table : hall) {
            System.out.println(table);
        }
    }


}
