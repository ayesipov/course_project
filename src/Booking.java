import java.time.LocalDateTime;

public class Booking {

    private final LocalDateTime from;
    private final LocalDateTime to;
    private final String descriptionForBooking;
    private final User user;


    public Booking(LocalDateTime from, LocalDateTime to, String descriptionForBooking, User user) {
        this.from = from;
        this.to = to;
        this.descriptionForBooking = descriptionForBooking;
        this.user = user;
    }

    public LocalDateTime getFrom() {
        return from;
    }

    public String getDescriptionForBooking() {
        return descriptionForBooking;
    }

    public User getUser() {
        return user;
    }

    public LocalDateTime getTo() {
        return to;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "from=" + from +
                ", to=" + to +
                ", descriptionForBooking='" + descriptionForBooking + '\'' +
                ", user=" + user +
                '}';
    }
}
