public class User {
    private final String name;
    private final String phoneNumber;

    public User(String name, String phoneNumber) {
        if (name.isEmpty()) {
            throw new IllegalArgumentException("User`s name can`t be empty");
        }
        if (phoneNumber.isEmpty()) {
            throw new IllegalArgumentException("User`s phoneNumber can`t be empty");
        }
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}' + "\n";
    }
}
