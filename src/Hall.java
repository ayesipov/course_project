import java.util.Iterator;
import java.util.List;

public class Hall implements Iterable<Table> {

    private final String hallName;
    private List<Table> tables;


    public Hall(String hallName, List<Table> tables) {
        if (tables.isEmpty()) {
            throw new IllegalArgumentException("Tables can not be Empty");
        }
        this.hallName = hallName;
        this.tables = tables;

        int i = 1;
        for (Table table : tables) {
            table.setTableNumber(i);
            i++;
        }
    }

    public void addTable(Table table) {
        table.setTableNumber(tables.size() + 1);
        tables.add(table);
    }

    public String getHallName() {
        return hallName;
    }

    public List<Table> getTables() {
        return tables;
    }

    @Override
    public Iterator<Table> iterator() {
        return tables.iterator();
    }
}
