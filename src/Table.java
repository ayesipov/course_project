import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Table {

    private final int countOfSeats;
    private int tableNumber;
    private List<Booking> bookings = new ArrayList<>();

    public Table(int countOfSeats) {
        this.countOfSeats = countOfSeats;
    }

    public int getCountOfSeats() {
        return countOfSeats;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public void addBooking(Booking booking) {
        this.bookings.add(booking);
    }


    public int getTableNumber() {
        return tableNumber;
    }

    private boolean timeInRange(LocalDateTime start1, LocalDateTime end1, LocalDateTime start2, LocalDateTime end2) {
        return ((start1.isBefore(start2)
                && (end1.isBefore(start2) || end1.isEqual(start2)))
                || (start1.isAfter(end2)) || start1.isEqual(end2));
    }

    public boolean isFree(LocalDateTime start, LocalDateTime end) {
        for (Booking booking : bookings) {
            if (!timeInRange(start, end, booking.getFrom(), booking.getTo())) {
                return false;
            }
        }
        return true;

    }

    @Override
    public String toString() {
        return "Table{" +
                "countOfSeats=" + countOfSeats +
                ", tableNumber=" + tableNumber +
                ", bookings=" + bookings +
                '}';
    }
}
